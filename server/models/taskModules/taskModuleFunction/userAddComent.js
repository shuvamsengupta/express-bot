const axios = require("axios");
const { ticketApplication } = require("../../../../config/config");
const url = ticketApplication.baseUrl;

module.exports.userAddComent = async (payload, userid, ticketid) => {
  console.log("userAddComent", payload, userid, ticketid);
  try {
    const response = await axios.put(
      `${url}/User/${userid}/ticket/${ticketid}/comment`,
      payload
    );
    if (response && response.data) {
      console.log(response.data);
      return {
        status: "Success",
        data: response.data,
      };
    }
  } catch (error) {
    if (error.response) {
      // console.log(error.response);
      return {
        status: "Failed",
        error: error.response.data,
      };
    }
  }
};
