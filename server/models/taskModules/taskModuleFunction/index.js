module.exports = { 
    raiseTicket : require('./raiseTicket').raiseTicket,
    updateTicket: require('./updateTicket').updateTicket,
    userAddComment : require('./userAddComent').userAddComent,
    agentAddComment : require('./agentAddComment').agentAddComment,
    updateTicketStatus : require('./updateTicketStatus').updateTicket,
}