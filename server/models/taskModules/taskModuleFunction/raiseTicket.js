const axios = require("axios");
const { ticketApplication } = require("../../../../config/config");
const {success,error} = require('consola')
const url = ticketApplication.baseUrl;

module.exports.raiseTicket = async (payload, user_id) => {
  // console.log("user API", user_id, payload);
  try {
    const response = await axios.post(`${url}/ticket/${user_id}`, payload);
    if (response && response.data) {
      success({
        message : "Ticket Raised Successfully",
        badge : true
      })
      return {
        status: "Success",
        data: response.data,
      };
    }
  } catch (err) {
    if (err.response) {
      error({
        message : "Unable To Raise Ticket",
        badge : true
      })
      return {
        status: "Failed",
        error: err.response.data.error,
      };
    }
  }
};
