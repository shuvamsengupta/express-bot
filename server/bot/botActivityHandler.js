const {
  TeamsActivityHandler,
  CardFactory,
  MessageFactory,
} = require("botbuilder");
const { Welcome } = require("../cards/welcome");
const {
  TaskModuleResponseFactory,
} = require("../models/taskModules/taskmoduleresponsefactory");

class BotActivityHandler extends TeamsActivityHandler {
  constructor() {
    super();

    this.onMessage(async (context, next) => {
      await context.sendActivity(context.activity.text);
      await context.sendActivity({
        attachments: [CardFactory.adaptiveCard(Welcome())],
      });
      await next();
    });
  }

  async handleTeamsTaskModuleFetch(context, taskModuleRequest) {
    try {
      let cardTaskFetchValue = taskModuleRequest.data.data;
      let taskInfo = {};
      if (cardTaskFetchValue === "UI") {
        taskInfo.url = taskInfo.fallbackUrl =
          "https://6e1e-49-37-35-241.ngrok.io/home";
        this.setTaskInfo(taskInfo, "UI");
      }
      return TaskModuleResponseFactory.toTaskModuleResponse(taskInfo);
    } catch (error) {
      console.log(error);
    }
  }

  setTaskInfo(taskInfo, uiSettings) {
    taskInfo.height = uiSettings.height;
    taskInfo.width = uiSettings.width;
    taskInfo.title = uiSettings.title;
  }
}

module.exports.BotActivityHandler = BotActivityHandler;
