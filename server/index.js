const path = require("path");
const express = require("express");
const cors = require("cors");
const ENV_FILE = path.join(__dirname, "../.env");
require("dotenv").config({ path: ENV_FILE });

const PORT = process.env.PORT || 1500;
const server = express();

let filePath = path.join(__dirname, "../public");
server.use(express.static(filePath));

server.use(cors());
server.set("view engine", "ejs");
server.use(express.json());
server.use(
  express.urlencoded({
    extended: true,
  })
);

server.use("/api", require("./api"));
server.get("/home", (req, res) => {
  let token = true;
  if (token) {
    res.render("home", {
      isLoggedIn: true,
    });
  } else {
    res.render("home", {
      isLoggedIn: false,
    });
  }
});

server.listen(1500, () => {
  console.log(`Server listening on http://localhost:${PORT}`);
});
