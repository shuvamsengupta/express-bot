const { BotFrameworkAdapter } = require("botbuilder");
const { BotActivityHandler } = require("../bot/botActivityHandler");

const adapter = new BotFrameworkAdapter({
  appId: process.env.MicrosoftAppId,
  appPassword: process.env.MicrosoftAppPassword,
});

adapter.onTurnError = async (context, error) => {
  console.error(`\n [onTurnError] unhandled error: ${error}`);
  await context.sendTraceActivity(
    "OnTurnError Trace",
    `${error}`,
    "https://www.botframework.com/schemas/error",
    "TurnError"
  );
  await context.sendActivity(
    "The bot encountered an error or bug, please fix the bot source code."
  );
};

const botActivityHandler = new BotActivityHandler();
const botHandler = (req, res) => {
  adapter.processActivity(req, res, async (context) => {
    await botActivityHandler.run(context);
  });
};

module.exports = botHandler;
